;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games build mojo-build-system)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module ((nonguix build binary-build-system) #:prefix binary:)
  #:use-module (nonguix build utils)
  #:use-module (guix build utils)
  #:use-module (ice-9 rdelim)
  #:export (%standard-phases
            mojo-build))

;; Commentary:
;;
;; Builder-side code of the build procedure for MojoSetup games, like those
;; from GOG.com.
;;
;; Code:

(define* (unpack #:key source #:allow-other-keys)
  "Unpack SOURCE in the working directory, and change directory within the
source."
  (invoke "makeself_safeextract" "--mojo" source)
  (chdir ((@@ (guix build gnu-build-system) first-subdirectory) "."))
  #t)

(define (patchelf . args)
  "Change directory before running binary-build-systems's patchelf phase.
Often enables Mojo packages to share another variant's patchelf-plan."
  (with-directory-excursion "data/noarch/game"
    (apply (@@ (nonguix build binary-build-system) patchelf) args)))

(define (string-drop-prefix str prefix)
  "Remove PREFIX from the start of STR, if necessary, and return it."
  (if (string-prefix? prefix str)
      (string-drop str (string-length prefix))
      str))

(define* (install #:key inputs outputs #:allow-other-keys)
  "Copy files from the \"source\" build input to the \"out\" output according
to a standard layout for Mojo games.  Also place a wrapper script in PATH to
launch the game, and generate a desktop entry file."
  (let* ((out (assoc-ref outputs "out"))
         (package (strip-store-file-name out))
         (name (package-name->name+version
                (string-drop-prefix package "gog-")))
         (wrapper (string-append out "/bin/" name))
         (prefix (string-append out "/share/" package))
         (title (call-with-input-file "data/noarch/gameinfo"
                  read-line))
         (command (string-append prefix "/start.sh"))
         (icon (string-append prefix "/support/icon.png")))
    (mkdir-p prefix)
    (copy-recursively "data/noarch" prefix)
    (make-wrapper wrapper command)
    (make-desktop-entry-file
     (string-append out "/share/applications/" name ".desktop")
     #:name title
     #:exec command
     #:icon icon
     #:categories '("Application" "Game"))
    #t))

(define %standard-phases
  ;; Everything is as with the binary build system except for the `unpack',
  ;; `patchelf', and `install' phases.
  (modify-phases binary:%standard-phases
    (replace 'unpack unpack)
    (replace 'patchelf patchelf)
    (replace 'install install)))

(define* (mojo-build #:key inputs (phases %standard-phases)
                     #:allow-other-keys #:rest args)
  "Build the given package, applying all of PHASES in order."
  (apply gnu:gnu-build #:inputs inputs #:phases phases args))

;;; mojo-build-system.scm ends here
