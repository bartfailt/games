;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020, 2021, 2023 Efraim Flashner <efraim@flashner.co.il>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages diablo)
  #:use-module ((nonguix licenses) #:prefix license:)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system go)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages image)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph))

(define-public devilutionx
  (package
    (name "devilutionx")
    (version "1.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/diasurgical/devilutionX"
                            "/releases/download/"
                            version "/devilutionx-src.tar.xz"))
        (sha256
         (base32
          "17ys24866pa0vp7579d8s2fw50709bn9rinvyvlmlnkvpca582pq"))
        (modules '((guix build utils)))
        (snippet
         '(begin
            ;; These three are expected to be vendored or downloaded at build
            ;; time.  libmpq, libsmackerdec and devilutionx are all maintained
            ;; by the same group so unvendoring them seems unnecessary.
            (delete-file-recursively "dist/asio-src")
            ;(delete-file-recursively "dist/libmpq-src")
            ;(delete-file-recursively "dist/libsmackerdec-src")
            (delete-file-recursively "dist/libzt-src")
            (delete-file-recursively "dist/sdl_audiolib-src")
            (delete-file-recursively "dist/simpleini-src")
            (delete-file "dist/devilutionx.mpq")))))
    (build-system cmake-build-system)
    (arguments
     `(#:build-type "Release"           ; Specifically suggested.
       #:configure-flags
       (list "-DBUILD_ASSETS_MPQ=ON"
             "-DDISABLE_ZERO_TIER=ON"   ; Under the Business Source License.
             (string-append "-DDEVILUTIONX_ASSETS_OUTPUT_DIRECTORY="
                            (assoc-ref %outputs "out")
                            "/share/diasurgical/devilutionx/assets"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'unbundle-asio
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* "CMake/Dependencies.cmake"
               (("add_subdirectory\\(3rdParty/asio\\)")
                (string-append "add_library(asio INTERFACE)\n"
                               "  target_include_directories(asio INTERFACE "
                               (assoc-ref inputs "asio") "/include)")))))
         (add-after 'unpack 'skip-some-tests
           (lambda _
             (substitute* "test/pack_test.cpp"
               (("CompareItems.*DiabloItems" all) (string-append "// " all))
               (("CompareItems.*SpawnItems" all) (string-append "// " all))
               (("CompareItems.*HellfireItems" all) (string-append "// " all)))))
         ;; The binary expects the assets to be in the same folder as the binary.
         (add-after 'install 'adjust-install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (rename-file
                 (string-append out "/bin/devilutionx")
                 (string-append out "/share/diasurgical/devilutionx/devilutionx"))
               (symlink
                 "../share/diasurgical/devilutionx/devilutionx"
                 (string-append out "/bin/devilutionx"))))))))
    (native-inputs
     `(("asio" ,asio)
       ("gettext" ,gettext-minimal)
       ("googletest" ,googletest)
       ("pkg-config" ,pkg-config)
       ("simpleini" ,simpleini)
       ("smpq" ,smpq)))
    (inputs
     `(("libsodium" ,libsodium)
       ("libpng" ,libpng)
       ("sdl2" ,sdl2-2.0)
       ("sdl2-audiolib" ,sdl-audiolib)
       ("sdl2-image" ,sdl2-image)))
    (home-page "https://github.com/diasurgical/devilutionX")
    (synopsis "Diablo build for modern operating systems")
    (description "DevilutionX is a source port of the classic action
role-playing hack and slash PC games Diablo and Hellfire that strives to make it
simple to run the game while providing engine inprovements, bugfixes, and some
optional quality of life features.")
    (license (list license:unlicense
                   license:gpl2          ; dist/libmpq-src
                   license:lgpl2.1+))))  ; dist/libsmackerdec-src

(define-public simpleini
  (package
    (name "simpleini")
    (version "4.19")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/brofield/simpleini")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32 "0i3ink4rlifmnqa008mh6dnr4kk9pivhxj5xayw97gp5n9p20wfs"))))
    (build-system gnu-build-system)
    (arguments
     (list
       #:phases
       #~(modify-phases %standard-phases
           (delete 'configure)          ; No configure script.
           (delete 'build)              ; Nothing to build.
           (replace 'check
             (lambda* (#:key tests? #:allow-other-keys)
               (when tests?
                 (with-directory-excursion "tests"
                   (invoke "make" "all")
                   (invoke "make" "test")))))
           ;; The install phase is fixed after this release.
           (replace 'install
             (lambda _
               (install-file "SimpleIni.h"
                             (string-append #$output "/include")))))))
    (native-inputs
     (list googletest pkg-config))
    (home-page "https://github.com/brofield/simpleini")
    (synopsis "C++ library to read and write INI files")
    (description "This package provides a library to read and write INI-style
configuration files.  It supports data files in ASCII, MBCS and Unicode.  It is
designed explicitly to be portable to any platform.")
    (license license:expat)))

(define-public stormlib
  (package
    (name "stormlib")
    (version "9.24")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ladislav-zezula/StormLib")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32 "13gjrvmbirsyj8r5syvgwmb34cvpf52kgx9kczj2n58mzki6m507"))
        (snippet
         #~(begin
             (use-modules (guix build utils))
             (delete-file-recursively "src/bzip2")
             ;; Even if we use the packaged libtomcrypt we still need
             ;; headers and source files from here.
             ;(delete-file-recursively "src/libtomcrypt")
             ;(delete-file-recursively "src/libtommath")
             (delete-file-recursively "src/zlib")))))
    (build-system cmake-build-system)
    (arguments
     (list
       #:tests? #f              ; Tests require many unbundled files.
       #:configure-flags
       #~(list ;; Undefined reference to many mp_* variables.
               ;; Our libtomcrypt is too new for stormlib.
               ;"-DWITH_LIBTOMCRYPT=ON"
               "-DBUILD_SHARED_LIBS=ON")))
    (native-inputs
     (list pkg-config))
    (inputs
     (list
       bzip2
       ;libtomcrypt
       ;libtommath
       zlib))
    (home-page "https://github.com/ladislav-zezula/StormLib")
    (synopsis "Library for accessing the MPQ archives")
    (description
     "The StormLib library is a pack of modules, written in C++, which are able
to read and write files from/to the MPQ archives.  MPQ is an archive format
developed by Blizzard Entertainment, purposed for storing data files, images,
sounds, music and videos for their games.")
    (license license:expat)))

(define-public smpq
  (package
    (name "smpq")
    (version "1.6")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://launchpad.net/smpq/trunk/" version
                            "/+download/smpq_" version ".orig.tar.gz"))
        (sha256
         (base32 "1jqq5x3b17jy66x3kkf5hs5l322dx2v14djxxrqrnqp8bn5drlmm"))))
    (build-system cmake-build-system)
    (arguments
     (list
       #:tests? #f              ; No test target.
       #:configure-flags
       #~(list "-DWITH_KDE=OFF")))
    (native-inputs
     (list stormlib))
    (home-page "https://launchpad.net/smpq")
    (synopsis "StormLib MPQ archiving utility")
    (description
     "SMPQ is the StormLib MPQ command line archiving utility.  This utility is
designed for full manipulating with Blizzard MPQ archives, supporting creating,
extracting, appending, renaming, deleting files in MPQ archives.  SMPQ can
access different types and versions of MPQ archives.  It supports encrypted,
compressed, partial and patched MPQ archives in version 1-4.")
    (license license:expat)))

;; We follow the needs of devilutionx since it is the only consumer of this package.
(define-public sdl-audiolib
  (let ((commit "cc1bb6af8d4cf5e200259072bde1edd1c8c5137e")
        (revision "1"))
    (package
      (name "sdl-audiolib")
      (version (git-version "0.0.0" revision commit))
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://github.com/realnc/SDL_audiolib")
                 (commit commit)))
          (file-name (git-file-name name version))
          (sha256
           (base32 "1pfp4zz73zyzz8wwmnflnvqb3ycylyw6912h9rgab4hf1jbymzn4"))
          (snippet
           #~(begin
               (use-modules (guix build utils))
               (delete-file-recursively "3rdparty/fmt")))))
      (build-system cmake-build-system)
      (arguments
       `(#:tests? #f  ; No test target.
         #:configure-flags (list "-DUSE_RESAMP_SRC=OFF"
                                 "-DUSE_RESAMP_SOXR=OFF"
                                 "-DUSE_DEC_DRWAV=ON"
                                 "-DUSE_DEC_DRFLAC=OFF"
                                 "-DUSE_DEC_OPENMPT=OFF"
                                 "-DUSE_DEC_XMP=OFF"
                                 "-DUSE_DEC_MODPLUG=OFF"
                                 "-DUSE_DEC_SNDFILE=OFF"
                                 "-DUSE_DEC_LIBVORBIS=OFF"
                                 "-DUSE_DEC_LIBOPUSFILE=OFF"
                                 "-DUSE_DEC_MUSEPACK=OFF"
                                 "-DUSE_DEC_FLUIDSYNTH=OFF"
                                 "-DUSE_DEC_BASSMIDI=OFF"
                                 "-DUSE_DEC_WILDMIDI=OFF"
                                 "-DUSE_DEC_ADLMIDI=OFF"
                                 "-DUSE_DEC_DRMP3=ON"
                                 ;; Build with sdl2
                                 "-DWITH_SYSTEM_FMTLIB=ON"
                                 "-DUSE_SDL1=OFF")))
      (native-inputs
       (list pkg-config))
      (propagated-inputs
       (list flac fmt-8 mpg123))
      (inputs
       (list sdl2))
      (home-page "https://github.com/realnc/SDL_audiolib")
      (synopsis "Audio decoding, resampling and mixing library for SDL")
      (description "This is a small and C++ library for playing various audio
formats.  It is a thin (-ish) wrapper around existing resampling (like SRC or
SoX) and decoding libraries (like libmpg123 or libvorbis).")
      (license (list license:lgpl3 license:gpl3)))))

(define diablosw.exe
  (origin
    (method url-fetch)
    (uri "http://ftp.blizzard.com/pub/demos/diablosw.exe")
    (sha256
     (base32
      "1dnkcbwajbijlhh0fq6wgw81pxixgbs8qk0qn0vaj0q3hl3drfyj"))))

(define-public diablo-spawn
  (package
    (name "diablo-spawn")
    (version "0")
    (source #f)
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (let* ((out          (assoc-ref %outputs "out"))
                (bin          (string-append out "/bin"))
                (diablosw.exe (assoc-ref %build-inputs "diablosw.exe"))
                (spawn-mpq    (assoc-ref %build-inputs "spawn-mpq"))
                (devilutionx  (assoc-ref %build-inputs "devilutionx"))
                (desktop      (string-append out "/share/applications"))
                (share        (string-append out "/share/diasurgical/devilution/")))
           (use-modules (guix build utils))
           (mkdir-p bin)
           (mkdir-p desktop)
           (mkdir-p share)
           (invoke (string-append spawn-mpq "/bin/spawn_mpq")
                   "-dir" share diablosw.exe)
           (call-with-output-file
             (string-append bin "/spawn")
             (lambda (file)
               (format file
                       "#!/bin/sh
~a/bin/devilutionx --data-dir ~a $@\n"
                       devilutionx share)))
           (chmod (string-append bin "/spawn") #o555)

           (make-desktop-entry-file
             (string-append desktop "/spawn.desktop")
             #:name "spawn"
             #:comment "Diablo I Demo"
             #:exec (string-append bin "/spawn")
             #:categories '("Application" "Game"))))))
    (inputs
     `(("devilutionx" ,devilutionx)))
    (native-inputs
     `(("diablosw.exe" ,diablosw.exe)
       ("spawn-mpq" ,spawn-mpq)))
    (home-page "https://www.blizzard.com/en-us/games/legacy/")
    (synopsis "Diablo I demo")
    (description "Diablo is an action role-playing hack and slash video game
developed by Blizzard North and released by Blizzard Entertainment in early
January 1997.  Set in the fictional Kingdom of Khanduras in the mortal realm,
Diablo makes the player take control of a lone hero battling to rid the world of
Diablo, the Lord of Terror.  Beneath the fictional town of Tristram, the player
journeys through sixteen randomly generated dungeon levels, ultimately entering
Hell itself in order to face Diablo.

This is the public demo.")
    (license (license:nonfree "No URL"))))

(define-public diablo
  (package
    (name "diablo")
    (version "1.09")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://archive.org/download/Diablo_1996_Blizzard"
                            "/Diablo (1996)(Blizzard).iso"))
        (file-name "Diablo.iso")
        (sha256
         (base32 "1na7qxq6i48d34yl1zbkv3cf4p170ki66c7sjiaxxbq7mx0x0idp"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (let* ((out         (assoc-ref %outputs "out"))
                (bin         (string-append out "/bin"))
                (bsdtar      (string-append (assoc-ref %build-inputs "libarchive")
                                            "/bin/bsdtar"))
                (iso         (assoc-ref %build-inputs "source"))
                (devilutionx (assoc-ref %build-inputs "devilutionx"))
                (icon        "/share/icons/hicolor/512x512/apps/devilutionx.png")
                (desktop     (string-append out "/share/applications"))
                (share       (string-append out "/share/diasurgical/devilution/")))
           (use-modules (guix build utils))
           (mkdir-p bin)
           (mkdir-p desktop)
           (mkdir-p share)
           (mkdir-p (dirname (string-append out icon)))
           (symlink (string-append devilutionx icon)
                    (string-append out icon))
           (invoke bsdtar "xf" iso "DIABDAT.MPQ")
           (install-file "DIABDAT.MPQ" share)
           (call-with-output-file
             (string-append bin "/diablo")
             (lambda (file)
               (format file
                       "#!/bin/sh
~a/bin/devilutionx --data-dir ~a $@\n"
                       devilutionx share (string-append out))))
           (chmod (string-append bin "/diablo") #o555)

           (make-desktop-entry-file
             (string-append desktop "/diablo.desktop")
             #:name "diablo"
             #:comment "Diablo I"
             #:exec (string-append bin "/diablo")
             #:icon (string-append out icon)
             #:categories '("Application" "Game"))))))
    (native-inputs
     `(("libarchive" ,libarchive)))
    (inputs
     `(("devilutionx" ,devilutionx)))
    (home-page "https://www.blizzard.com/en-us/games/legacy/")
    (synopsis "Diablo I")
    (description "Diablo is an action role-playing hack and slash video game
developed by Blizzard North and released by Blizzard Entertainment in early
January 1997.  Set in the fictional Kingdom of Khanduras in the mortal realm,
Diablo makes the player take control of a lone hero battling to rid the world of
Diablo, the Lord of Terror.  Beneath the fictional town of Tristram, the player
journeys through sixteen randomly generated dungeon levels, ultimately entering
Hell itself in order to face Diablo.")
    (license (license:nonfree "No URL"))))

(define-public spawn-mpq
  (let ((commit "860ce57c4fba27e3bfb04f41faaba068464698bb") ; March 23, 2020
        (revision "1"))
    (package
      (name "spawn-mpq")
      (version (git-version "0.0.0" revision commit))
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://github.com/mewspring/spawn_mpq")
                 (commit commit)))
          (file-name (git-file-name name version))
          (sha256
           (base32
            "1nq7dqvjjyfgbsvbq0fiss2fyim71yqa1qs246y7k95qcp7yi3vh"))))
      (build-system go-build-system)
      (arguments
       '(#:tests? #f    ; No test files.
         #:install-source? #f
         #:import-path "github.com/mewspring/spawn_mpq"))
      (inputs
       `(("go-github-com-mewrev-pe" ,go-github-com-mewrev-pe)
         ("go-github-com-mewkiz-pkg-httputil" ,go-github-com-mewkiz-pkg-httputil)
         ("go-github-com-pkg-errors" ,go-github-com-pkg-errors)
         ("go-github-com-sanctuary-exp-mpq" ,go-github-com-sanctuary-exp-mpq)))
      (home-page "https://github.com/mewspring/spawn_mpq")
      (synopsis "Tool to extract spawn.mpq from diablosw.exe demo")
      (description
       "Spawn-mpq is a tool to extract spawn.mpq from diablosw.exe demo.")
      (license license:unlicense))))

(define-public go-github-com-mewkiz-pkg-httputil
  (let ((commit "518ade7978e2ce16b08e90878fb43cdeed230bde") ; Sept 19, 2019
        (revision "1"))
    (package
      (name "go-github-com-mewkiz-pkg-httputil")
      (version (git-version "0.0.0" revision commit))
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://github.com/mewkiz/pkg")
                 (commit commit)))
          (file-name (git-file-name name version))
          (sha256
           (base32
            "0qw7baq6189g48wgzbdp2ijvkjhmfs1fanzzr0fspn72nkbj548i"))))
      (build-system go-build-system)
      (arguments
       '(#:tests? #f    ; No test files.
         #:import-path "github.com/mewkiz/pkg/httputil"
         #:unpack-path "github.com/mewkiz/pkg"))
      (propagated-inputs
       `(("go-golang-org-x-net" ,go-golang-org-x-net)))
      (home-page "github.com/mewkiz/pkg")
      (synopsis "Small utility packages")
      (description "The pkg project provides packages for various utility
functions and commonly used features.")
      (license license:public-domain))))

(define-public go-github-com-mewrev-pe
  (let ((commit "8f6d1d7d219c750096c34991293583e4a6cb5a33") ; Oct. 24, 2018
        (revision "1"))
    (package
      (name "go-github-com-mewrev-pe")
      (version (git-version "0.0.0" revision commit))
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://github.com/mewrev/pe")
                 (commit commit)))
          (file-name (git-file-name name version))
          (sha256
           (base32
            "0zv7piadv3794bwg64r998i5zq5yx5ikxgzgmbr0qsxac6c8zsar"))))
      (build-system go-build-system)
      (arguments
       '(#:tests? #f    ; No test files.
         #:import-path "github.com/mewrev/pe"))
      (propagated-inputs
       `(("go-github-com-pkg-errors" ,go-github-com-pkg-errors)))
      (home-page "https://github.com/mewkiz/pkg")
      (synopsis "Access to the Portable Executable (PE) file format")
      (description "Package pe implements access to the @dfn{Portable
Executable} (PE) file format.")
      (license license:public-domain))))

(define-public go-github-com-sanctuary-exp-mpq
  (let ((commit "f737bea33cb613b69667f47fb38c03ba88aad007") ; Jan. 1, 2020
        (revision "1"))
    (package
      (name "go-github-com-sanctuary-exp-mpq")
      (version (git-version "0.0.0" revision commit))
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://github.com/sanctuary/exp")
                 (commit commit)))
          (file-name (git-file-name name version))
          (sha256
           (base32
            "1njr05gp3d0r2192b6bmlzls49r0iw71ncin0rhn88s0lcx6l56b"))))
      (build-system go-build-system)
      (arguments
       ;; Tests fail: src/github.com/sanctuary/exp/mpq/decrypt_test.go:17:10: undefined: hash
       '(#:tests? #f
         #:import-path "github.com/sanctuary/exp/mpq"
         #:unpack-path "github.com/sanctuary/exp"))
      (propagated-inputs
       `(("go-github-com-egonelbre-exp-bit" ,go-github-com-egonelbre-exp-bit)
         ("go-github-com-pkg-errors" ,go-github-com-pkg-errors)))
      (home-page "https://github.com/sanctuary/exp")
      (synopsis "Tools and libraries related to the Diablo 1 game engine")
      (description "Throw-away prototypes for experimental tools and libraries
related to the Diablo 1 game engine.")
      (license license:public-domain))))

(define-public go-github-com-egonelbre-exp-bit
  (let ((commit "e195833d0f10acae9805025ae800e80db097ed3e") ; Dec. 9, 2019
        (revision "1"))
    (package
      (name "go-github-com-egonelbre-exp-bit")
      (version (git-version "0.0.0" revision commit))
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://github.com/egonelbre/exp")
                 (commit commit)))
          (file-name (git-file-name name version))
          (sha256
           (base32
            "0fzk4xaiw4wqx9dwp3691c0rrid408mdq34ndh5z7ikfffj1vi47"))))
      (build-system go-build-system)
      (arguments
       '(#:import-path "github.com/egonelbre/exp/bit"
         #:unpack-path "github.com/egonelbre/exp"))
      (home-page "https://github.com/egonelbre/exp")
      (synopsis "Experiments that do not fit into a separate repository")
      (description
       "This package contains experimental Go code for use in other projects.")
      (license license:unlicense))))
