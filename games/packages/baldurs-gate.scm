(define-module (games packages baldurs-gate)
  #:use-module (games build-system mojo)
  #:use-module (guix packages)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages xml)
  #:use-module (past packages tls)
  #:use-module (games gog-download)
  #:use-module (guix transformations)
  #:use-module (nonguix licenses))

(define-public gog-baldurs-gate
  (package
    (name "gog-baldurs-gate")
    (version "2.6.6.0")
    (source
     (origin
       (method gog-fetch)
       (uri "gogdownloader://baldurs_gate_enhanced_edition/en3installer0")
       (file-name (string-append "baldur_s_gate_enhanced_edition-" version ".sh"))
       (sha256 (base32 "1v32cv25sdlsc5l227wwn0bzivcsiwxir4hpf9fxgy2qvpydyzks"))))
    (arguments `(#:patchelf-plan
                 `(("BaldursGate"
                    ("mesa"
                     "gcc:lib"
                     "openssl"
                     "openal"
                     "expat")))))
    (inputs
     `(("mesa" ,mesa)
       ("gcc:lib" ,gcc "lib")
       ("openssl" ,((options->transformation
                     `((without-tests . "openssl")))
                    openssl-1.0))
       ("openal" ,openal)
       ("expat" ,expat)))
    (build-system mojo-build-system)
    (home-page "https://www.gog.com/en/game/baldurs_gate_enhanced_edition")
    (synopsis "Baldur's Gate: Enhanced Edition")
    (description "Gather Your Party
Baldur's Gate: Enhanced Edition is a story-driven 90s RPG set in the world of Dungeons & Dragons.

Customize your hero, recruit a party of brave allies, and explore the Sword Coast in your search for adventure, profit… and the truth.

Note: If the cursor has a bugged texture, disable hardware cursor in the settings menu.")
    (license (undistributable "No URL"))))
