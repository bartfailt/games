;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;; Copyright © 2019 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages reus)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages image)
  #:use-module (games packages mono)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages video)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xorg)
  #:use-module (games utils)
  #:use-module (nonguix build utils)
  #:use-module (games humble-bundle)
  #:use-module (nonguix licenses))

(define libpng-1.5
  (package
    (inherit libpng)
    (version "1.5.30")
    (source
     (origin
       (method url-fetch)
       (uri (list (string-append "mirror://sourceforge/libpng/libpng15/"
                                 version "/libpng-" version ".tar.xz")
                  (string-append
                   "ftp://ftp.simplesystems.org/pub/libpng/png/src"
                   "/libpng15/libpng-" version ".tar.xz")
                  (string-append
                   "ftp://ftp.simplesystems.org/pub/libpng/png/src/history"
                   "/libpng15/libpng-" version ".tar.xz")))
       (sha256
        (base32
         "1vanlydv62l4za7rdp39b9ms3ll8ykk4dzf5hxylppifmmgjfxkx"))))))

(define-public reus
  (let* ((version "1.6.5")
         (file-name (string-append "reus-linux-" version ".tar.gz"))
         (binary (match (or (%current-target-system)
                            (%current-system))
                   ("x86_64-linux" "Reus.bin.x86_64")
                   ("i686-linux" "Reus.bin.x86")
                   (_ "")))
         (lib-dir (if (target-64bit?)
                    "lib64"
                    "lib"))
         (other-lib-dir (if (target-64bit?)
                          "lib"
                          "lib64")))
    (package
      (name "reus")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "124f9lrg4apbr4wci2x7z6jk9bh5f75gp6bvi3fnzvm77iwa200s"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("gcc"))
           (,,(string-append lib-dir "/libfmodevent64-4.44.00.so")
            ("gcc"))
           (,,(string-append lib-dir "/libfmodex64-4.44.00.so")
            ("gcc"))
           (,,(string-append lib-dir "/libFontNative.so")
            ("freetype")))
         #:install-plan
         `(("." (".") "share/reus/")
           ("." ("Reus.bmp") "share/pixmaps/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'deal-with-tarbomb
             (lambda _
               (chdir "..")
               #t))
           (add-after 'install 'cleanup-install
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (game-dir (string-append output "/share/reus"))
                      (lib (string-append game-dir "/" ,lib-dir))
                      (lib-temp (string-append game-dir "/lib-temp")))
                 (delete-file-recursively (string-append game-dir "/" ,other-lib-dir))
                 (mkdir-p lib-temp)
                 (for-each (lambda (f)
                             (rename-file f (string-append lib-temp "/" (basename f))))
                           (find-files lib "fmod|FontNative"))
                 (delete-file-recursively lib)
                 (rename-file lib-temp lib)
                 (delete-file (string-append game-dir "/Reus")))
               #t))
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/reus"))
                      (real (string-append output "/share/reus/" ,binary)))
                 (make-wrapper wrapper real
                               `("LD_LIBRARY_PATH" ":" prefix
                                 ,(append
                                   (map (lambda (lib) (string-append (assoc-ref inputs lib) "/lib"))
                                        '("sdl2" "sdl2-image" "mojoshader" "mono" "libvorbis" "libtheora"
                                          "libogg" "openal" "libpng" "theorafile"))
                                   ;; For FMOD and FontNative.
                                   (list (string-append output "/share/reus/" ,lib-dir)))))
                 (make-desktop-entry-file (string-append output "/share/applications/reus.desktop")
                                          #:name "Reus"
                                          #:exec wrapper
                                          #:icon (string-append output "/share/pixmaps/Reus.bmp")
                                          #:comment "A god game in which you take control of nature through the hands of mighty giants"
                                          #:categories '("Application" "Game")))
               #t)))))
      (inputs
       `(("gcc" ,gcc "lib")
         ("sdl2" ,sdl2)
         ("sdl2-image" ,sdl2-image)
         ("mojoshader" ,mojoshader-with-viewport-flip)
         ("mono" ,mono)
         ("libvorbis" ,libvorbis)
         ("libtheora" ,libtheora)
         ("theorafile" ,theorafile)
         ("libogg" ,libogg)
         ("openal" ,openal)
         ("libpng" ,libpng-1.5)
         ("freetype" ,freetype)))
      (home-page "https://www.reusgame.com/")
      (synopsis "Control giants and modify the nature of the planet")
      (description "Reus is a god game by Abbey Games in which you take control
of nature through the hands of mighty giants.  You possess all imaginable powers
over nature!  There is only one thing on the planet that you do not control:
mankind, with all their virtues and and all their vices.  You can shape their
world, but not their will.  It’s your responsibility to maintain a balance in
which man is not overpowered by nature, and nature does not fall to man’s greed.

@itemize
@item Unique god game gameplay with several ancient giants at your command.
@item An interesting 2D art style from a perspective rarely found in god games.
@item Dynamic and immersive audio effects.
@item A powerful soundtrack that fits the theme.
@item Intricate gameplay, where humanity depends on your power while challenging
it at the same time.
@item \"Let your giants decide the fate of humanity.\"
@end itemize\n")
      (license (undistributable "No URL")))))

(define-public gog-reus
  (let* ((buildno "20844")
         (binary (match (or (%current-target-system)
                            (%current-system))
                   ("x86_64-linux" "Reus.bin.x86_64")
                   ("i686-linux" "Reus.bin.x86")
                   (_ "")))
         (lib-dir (match (or (%current-target-system)
                             (%current-system))
                    ("x86_64-linux" "lib64")
                    ("i686-linux" "lib")
                    (_ ""))))
    (package
      (inherit reus)
      (name "gog-reus")
      (version "1.6.5")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://reus/en3installer0")
         (file-name (string-append "reus_en_"
                                   (string-replace-substring version "." "_")
                                   "_" buildno ".sh"))
         (sha256
          (base32
           "17fdd4z6fpz0wcnb07yjdyj4h986697mrpcapl5r7xjaa5j8ad2f"))))
      (build-system mojo-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       (strip-keyword-arguments
        '(#:install-plan)
        (ensure-keyword-arguments
         (package-arguments reus)
         `(#:phases
           (modify-phases %standard-phases
             (add-after 'install 'wrap-game
               (lambda* (#:key inputs outputs #:allow-other-keys)
                 (display ,lib-dir) (newline)
                 (let* ((out (assoc-ref outputs "out"))
                        (package (strip-store-file-name out))
                        (prefix (string-append out "/share/" package))
                        (wrapper (string-append prefix "/start.sh"))
                        (ld-lib-path
                         (append
                          (map (lambda (lib)
                                 (string-append (assoc-ref inputs lib) "/lib"))
                               '("sdl2" "sdl2-image" "mojoshader" "mono"
                                 "libvorbis" "libtheora" "libogg" "openal"
                                 "libpng"))
                          ;; For FMOD, FontNative and theorafile.
                          (list (string-append prefix "/game/" ,lib-dir)))))
                   (wrap-program wrapper
                     `("LD_LIBRARY_PATH" ":" prefix ,ld-lib-path)))
                 #t)))))))
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
