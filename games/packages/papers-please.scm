;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages papers-please)
  #:use-module (ice-9 match)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages pulseaudio)
  #:use-module (nongnu packages game-development)
  #:use-module (games humble-bundle))

(define-public papers-please
  (package
    (name "papers-please")
    (version "1.1.65")
    (source
     (origin
       (method humble-bundle-fetch)
       (uri (humble-bundle-reference
             (help (humble-bundle-help-message name))
             (config-path (list (string->symbol name) 'key))
             (files (list (string-append "papers-please_"
                                         version "_i386.tar.gz")))))
       (file-name (string-append "papers-please_" version
                                 "_i386.tar.gz"))
       (sha256
        (base32
         "1msijmgpi53ryyzx7ibgwsrdw0kwqd6xw1fd8y0mbdqwn392lmjh"))))
    (build-system binary-build-system)
    (supported-systems '("i686-linux" "x86_64-linux"))
    (arguments
     `(#:system "i686-linux"
       #:patchelf-plan
       `(("PapersPlease" ("gcc:lib"))
         ("lime.ndll" ("gcc:lib" "mesa"))
         ("regexp.dso" ("gcc:lib" "mesa"))
         ("std.dso" ("gcc:lib" "mesa"))
         ("zlib.dso" ("gcc:lib" "mesa")))
       #:install-plan
       `(("." ("PapersPlease" "README" "boot.xml" "lime.ndll"
               "manifest" "regexp.dso" "std.dso" "zlib.dso")
          "share/papers-please/")
         ("assets" (".") "share/papers-please/assets/")
         ("loc" (".") "share/papers-please/loc/"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'install 'make-wrapper
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((output (assoc-ref outputs "out"))
                    (wrapper (string-append output "/bin/papers-please"))
                    (real (string-append output "/share/papers-please/PapersPlease"))
                    (icon-source (assoc-ref inputs "papers-please-icon"))
                    (icon (string-append output "/share/papers-please/PapersPlease.png")))
               (copy-file icon-source icon)
               (make-wrapper wrapper real
                             `("LD_LIBRARY_PATH" ":" prefix
                               ,(list (string-append (assoc-ref inputs "pulseaudio") "/lib"))))
               (make-desktop-entry-file
                (string-append output "/share/applications/papers-please.desktop")
                #:name "Papers, Please"
                #:exec wrapper
                #:icon icon
                #:categories '("Application" "Game")))
             #t)))))
    (inputs
     `(("gcc:lib" ,gcc "lib")
       ("mesa" ,mesa)
       ("pulseaudio" ,pulseaudio)
       ("papers-please-icon"
        ,(origin
           (method url-fetch)
           (uri "https://images.gog.com/85476bab1039d78b049a84c489ce522b23338790da34db2de926c82a0a496719.png")
           (sha256
        (base32
         "1fc6py3s3zj52qzir39bg2qdrwm8a8lcy42yx7797sxjq6v3lfvf"))))))
    (home-page "https://papersplea.se/")
    (synopsis "Border-crossing immigration officer simulation game")
    (description
     "In Papers, Please, the player takes on the role of a border-crossing
immigration officer in the fictional dystopian Eastern Bloc-like country of
Arstotzka, which has been and continues to be at political hostilities with its
neighboring countries.

The game takes place at a migration checkpoint in Grestin, a border city split
between Arstotzka and the neighboring country of Kolechia, and a fictional
parallel of the Cold War-era division between East and West Berlin.  As the
immigration officer, the player must review each immigrant and return citizen's
passports and other supporting paperwork against an ever-growing list of rules
using a number of tools and guides, allowing in only those with the proper
paperwork while rejecting those without all proper forms, and at times detaining
those with falsified information.  The player is rewarded in their daily salary
for how many people they have processed correctly in that day, while also being
fined for making mistakes; the salary is used to help provide shelter, food and
heat for the player's in-game family.  In some cases, the player will be
presented with moral decisions, such as approving entry of a pleading spouse of
a citizen despite the lack of proper paperwork, knowing this will affect their
salary.  The game deals with the issues of keeping up with immigration policy in
an ever-changing political environment.  In addition to a story mode which
follows several scripted events that occur within Arstotzka, the game includes
an endless mode that challenges the player to process as many immigrants as
possible.")
    (license (undistributable "file://LICENSE"))))
